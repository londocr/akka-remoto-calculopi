package com.ciriscr.akka.pi.server

import akka.actor.{Props, ActorSystem}

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 21/09/12
 * Time: 03:25 PM
 */

object Inicio extends App {
  val sistema = ActorSystem("Piremoto")
  val a = sistema.actorOf(Props[Master], "master")
  println("servidor iniciado")
}
