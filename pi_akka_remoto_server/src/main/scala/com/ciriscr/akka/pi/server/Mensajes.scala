package com.ciriscr.akka.pi.server

import akka.util.Duration

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 13/09/12
 * Time: 05:16 PM
 */

trait PiMessage

case class StarWork(hash: Int) extends PiMessage
case class Work(start: Int, cantElementos: Int) extends PiMessage

case class Result(value: Double) extends PiMessage
case class FinalResult(value: Double, duration: Duration, hash: Int) extends PiMessage
