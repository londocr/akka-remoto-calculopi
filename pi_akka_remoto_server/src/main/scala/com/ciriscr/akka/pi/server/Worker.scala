package com.ciriscr.akka.pi.server

import akka.actor.Actor

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 13/09/12
 * Time: 05:16 PM
 */

class Worker extends Actor {
  def receive = {
    case Work(start, nrOfElements) =>
      sender ! Result(calculatePiFor(start, nrOfElements)) // perform the work
  }

  def calculatePiFor(start: Int, nrOfElements: Int): Double = {
    val acc = for (i <- start until (start + nrOfElements)) yield
      (1 - (i % 2) * 2.0) / (2 * i + 1)
    4.0 * acc.reduce(_ + _)
  }
}
