package com.ciriscr.akka.pi.server

import akka.actor._
import akka.routing.RoundRobinRouter
import akka.util.Duration
import java.util.concurrent.TimeUnit
import com.ciriscr.akka.pi.shared.{PiApproximation, Calculate}

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 13/09/12
 * Time: 05:18 PM
 */

class Master() extends Actor {

  import scala.collection.mutable.Map

  val mapa = Map.empty[Int, ActorRef]

  def receive = {
    case c: Calculate => {
      println("mensaje recibido")
      val id = c.hashCode()
      val cal = context.actorOf(Props(new Calculador(c.numWorkers, c.numMensajesPorActor, c.numElementos)))
      cal ! StarWork(id)
      println(sender)
      mapa += id -> sender
    }
    case FinalResult(value, dur, id) => {
      mapa.remove(id).get ! PiApproximation(value, dur)
    }
  }

}

class Calculador(numWorkers: Int, numMensajesPorActor: Int, numElementos: Int) extends Actor {

  val workerRouter = context.actorOf(Props[Worker].withRouter(RoundRobinRouter(numWorkers)), name = "workerRouter")
  val numResultados = numMensajesPorActor
  var res = 0
  var total = 0.0
  val ini = System.currentTimeMillis()
  var _padre: ActorRef = _
  var _id: Int = _

  def receive = {
    case StarWork(id) => {
      for (i <- 0 until (numResultados)) workerRouter ! Work(i * numElementos, numElementos)
      _padre = sender
      _id = id
    }
    case Result(value) => {
      total += value
      res += 1
      if (res == numResultados) {
        _padre ! FinalResult(total, Duration(System.currentTimeMillis() - ini, TimeUnit.MILLISECONDS), _id)
        self ! PoisonPill
      }
    }
  }

}