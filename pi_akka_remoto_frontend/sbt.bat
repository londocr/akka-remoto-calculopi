set SCRIPT_DIR=%~dp0
java -Dfile.encoding=UTF8 -Xmx900M -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=512m -jar "%SCRIPT_DIR%sbt-launch.jar" %*
