name := "InterfazPI"

version := "1.0.0"

organization := "com.ciriscr"

scalaVersion := "2.9.2"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

maxErrors := 30

pollInterval := 1000

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

scalacOptions ++= Seq("-deprecation", "-optimise", "-explaintypes")

parallelExecution := true

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "1.6.1" % "test",
  "com.typesafe.akka" % "akka-actor" % "2.0.3" % "compile->default",
  "com.typesafe.akka" % "akka-remote" % "2.0.3"
)

