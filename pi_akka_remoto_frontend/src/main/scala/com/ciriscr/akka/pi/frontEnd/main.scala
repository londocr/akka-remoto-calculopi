package com.ciriscr.akka.pi.frontEnd

import com.ciriscr.akka.pi.client.CalculadorPI
import com.ciriscr.akka.pi.shared.Calculate
import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 27/09/12
 * Time: 10:15 PM
 */

object Main extends App {

  inicio

  def inicio {
    println("pre")
    val sys = ActorSystem.create("frontEnd", ConfigFactory.load.getConfig("frente"))
    val ac = sys.actorOf(Props[Llamador], "llamador")
    ac ! Calculate(4, 1000, 1000)
//    CalculadorPI.getCalculador ! Calculate(4, 1000, 1000)
    println("pos")
  }

}
