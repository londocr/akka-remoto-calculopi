package com.ciriscr.akka.pi.frontEnd

import akka.actor.Actor
import com.ciriscr.akka.pi.shared.Calculate
import com.ciriscr.akka.pi.client.CalculadorPI

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 28/09/12
 * Time: 11:26 AM
 */

case class Llamador() extends Actor {

  def receive = {
    case c: Calculate => {
      println(CalculadorPI.getCalculador)
      CalculadorPI.getCalculador ! c
    }
    case s: String => println(s)
  }

}
