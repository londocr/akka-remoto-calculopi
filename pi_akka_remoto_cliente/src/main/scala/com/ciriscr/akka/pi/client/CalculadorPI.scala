package com.ciriscr.akka.pi.client

import akka.actor.{ActorRef, Props, Actor, ActorSystem}
import com.ciriscr.akka.pi.shared.{PIMensajeRemoto, Calculate, PiApproximation}
import java.util.concurrent.TimeUnit

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 21/09/12
 * Time: 07:22 PM
 */

object CalculadorPI {

  val sistema = ActorSystem("CalculadorPI")
//  val mensajes: Iterator[PIMensajeRemoto] = crearMensajes

  val getCalculador = sistema.actorOf(Props[Forwarder])

//  private def crearMensajes = {
//    List(
//      List.fill[Calculate](12)(Calculate(1, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(2, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(3, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(4, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(5, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(6, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(7, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(8, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(9, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(10, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(20, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(50, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(100, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(200, 4000, 4000)),
//      List.fill[Calculate](12)(Calculate(500, 4000, 4000))
//    ).flatten.toIterator
//  }

}


case class Forwarder() extends Actor {

  val ac = CalculadorPI.sistema.actorFor("akka://Piremoto@192.168.1.107:2552/user/master")
  val log = new Logger("res.pi")
  var send: ActorRef = _

  def receive = {
    case PiApproximation(v, d) => {
      send ! "\n\tPi approximation: \t\t%s\n\tCalculation time: \t%s".format(v, d)
      log.escribir(sender.toString())
//      log.escribir("%s".format(d.toMillis))
//      if (mensajes.hasNext)
//        self ! mensajes.next()
//      else
//        println("Calculos terminado")
    }
    case c:Calculate => {
      log.escribir(ac.toString())
      send = sender
      ac ! c
    }
  }
}