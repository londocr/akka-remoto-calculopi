package com.ciriscr.akka.pi.shared

import java.io.PrintWriter

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 23/09/12
 * Time: 11:05 AM
 */

class Logger(filepath: String) {

  private val out: PrintWriter = new PrintWriter(filepath)

  def escribir(mensaje: String){
    out.println(mensaje)
    flush
  }

  def escribir(e: Exception){
    escribir(e.getMessage)
    e.getStackTrace.foreach(t => escribir("\t at " + t.toString))
  }

  def cerrar {out.close()}
  def flush {out.flush()}

}
