package com.ciriscr.akka.pi.shared

import akka.util.Duration

/**
 * Created with IntelliJ IDEA.
 * User: londo
 * Date: 21/09/12
 * Time: 06:54 PM
 */

trait PIMensajeRemoto

case class Calculate(val numWorkers: Int, val numMensajesPorActor: Int, val numElementos: Int) extends PIMensajeRemoto

case class PiApproximation(pi: Double, duration: Duration) extends PIMensajeRemoto
