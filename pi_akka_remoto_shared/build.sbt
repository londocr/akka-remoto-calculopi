name := "AkkaRemotoShared"

version := "1.1"

organization := "com.ciriscr"

scalaVersion := "2.9.2"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

maxErrors := 30

pollInterval := 1000

javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

scalacOptions ++= Seq("-deprecation", "-optimise", "-explaintypes")

parallelExecution := true

libraryDependencies ++= Seq(
  "com.typesafe.akka" % "akka-actor" % "2.0.3"
)
